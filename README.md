<h1 align="center">Hi 👋, I'm Danrley Senegalha Pires</h1>

![Profile views](https://gpvc.arturio.dev/dansenpir)

<h3 align="left">I am passionate about technology and development. 🚀</h3>

<div align="left">
    <h3>About me:</h3>
        <p>• 👨🏽‍💻 I’m currently learning : <b>JavaScript and his frameworks. I am on my way to becoming a Full Stack Developer!</b></p>
        <p>• ℹ️ Hobbies : Music 🎧 | Watch streaming 📺 | Pets 🐈</p>
        <p>• 📫 How to reach me : <b>senegalha@protonmail.com</b></p>
</div><br>

<div align="left">
  <h3>Languages and tools I'm learning until now:</h3><br>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=HTML5&color=%23E34F26&style=for-the-badge&logo=html5&logoColor=whitesmoke" alt="HTML5"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=CSS3&color=%231572B6&style=for-the-badge&logo=css3&logoColor=whitesmoke" alt="CSS3"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=Javascript&color=%23F7DF1E&style=for-the-badge&logo=javascript&logoColor=grey" alt="Javascript"> </a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=REACT.JS&color=%2361DAFB&style=for-the-badge&logo=react&logoColor=grey" alt="REACT.JS"></a>
    <br><br>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=GIT&color=%23F05032&style=for-the-badge&logo=git&logoColor=whitesmoke" alt="GIT"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=GITHUB&color=%23181717&style=for-the-badge&logo=github&logoColor=whitesmoke" alt="GITHUB"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=FIGMA&color=%23552d84&style=for-the-badge&logo=figma&logoColor=whitesmoke" alt="FIGMA"></a>
</div>

<div>
  <h3>Things I wanna learn in the future:</h3><br>
	<a href="https://"><img src="https://img.shields.io/static/v1?label=&message=SASS&color=%23CC6699&style=for-the-badge&logo=sass&logoColor=whitesmoke" alt="SASS"></a>
	<a href="https://"><img src="https://img.shields.io/static/v1?label=&message=Vue.js&color=%23339933&style=for-the-badge&logo=vue.js&logoColor=whitesmoke" alt="VUE.JS"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=NODE.JS&color=%23339933&style=for-the-badge&logo=node.js&logoColor=whitesmoke" alt="NODE.JS"></a><br>
	<a href="https://"><img src="https://img.shields.io/static/v1?label=&message=postgre&color=blue&style=for-the-badge&logo=postgresql&logoColor=whitesmoke" alt="PostgreSQL"></a>
    <a href="https://"><img src="https://img.shields.io/static/v1?label=&message=MONGODB&color=%2347A248&style=for-the-badge&logo=mongodb&logoColor=whitesmoke" alt="MONGODB"></a>
</div>

<div>
  <h3>Social Medias:</h3><br>
    <a href="https://www.linkedin.com/in/dansenpir/" target="_blank"><img src="https://img.shields.io/static/v1?label=&message=Linkedin&color=darkblue&style=for-the-badge&logo=linkedin&logoColor=whitesmoke" alt="LinkedIn"></a>
    <a href="https://t.me/dansenpir/" target="_blank"><img src="https://img.shields.io/static/v1?label=&message=Telegram&color=blue&style=for-the-badge&logo=telegram&logoColor=black" alt="Telegram"></a>
</div>
